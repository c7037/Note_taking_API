from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
db = SQLAlchemy(app)


class Note(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), unique=True, nullable=False)
    content = db.Column(db.String(400))

    def __repr__(self):
        return f"{self.title}, {self.content}"

@app.route('/')
def index():
    return 'app is running'

# List the notes from DB
@app.route('/note_list')
def get_all_notes():
    note_list = Note.query.all()

    output = []
    for note in note_list:
        current_note = {'Note title': note.title, 'Content': note.content}
        output.append(current_note)
    return {"Notes": output}

# Get note
@app.route('/note/<id>')
def get_note(id):
    note = Note.query.get_or_404(id)
    return ({"Note title": note.title, 'Content': note.content})


# Post note
@app.route('/add_note', methods=['POST'])
def add_note():
    new_note = Note(title=request.json['title'], content=request.json['content'])
    db.session.add(new_note)
    db.session.commit()

    # Example curl post <curl -i -X POST -H 'Content-Type: application/json' -d '{"title":"Test note2", "content":"Clean the house"}' http://127.0.0.1:5000//add_note>
    return {'New note added under id ': new_note.id}


# Edit note
@app.route('/edit_note/<id>', methods=['PUT'])
def edit_note(id):
    # holder_note = Note(title=request.json['title'], content=request.json['content'])
    note_to_edit = Note.query.get_or_404(id)
    # note_to_edit['title'] = request.json['title']
    # note_to_edit['content'] = request.json['content']
    note_to_edit.title = request.json['title']    #Debugging
    note_to_edit.content = request.json['content']
    
    db.session.add(edit_note)
    db.session.commit()

    # Example curl put: <curl -i -H 'Content-Type: application/json' -X PUT -d '{"title":"Edited note3","content":"I have been updated"}' http://127.0.0.1:5000//edit_note/3">
    return {'Note has updated'}


# Delete note
@app.route('/delete_note/<id>', methods=['DELETE'])
def delete_note(id):
    old_note = Note.query.get_or_404(id)
    db.session.delete(old_note)
    db.session.commit()

    # Example curl delete <curl -X "DELETE" http://127.0.0.1:5000//delete_note/4>
    return 'Note has been deleted'


"""  Code Notes
Accesss points wanted create/list/view/edit/delete notes

Access points:
   Check flask is running:     http://127.0.0.1:5000/
   View the list of Notes:     http://127.0.0.1:5000/note_list
   View one note:              http://127.0.0.1:5000/note/1
   Post a note(terminal):      curl -i -X POST -H 'Content-Type: application/json' -d '{"title":"Test note2", "content":"Clean the house"}' http://127.0.0.1:5000//add_note
   Update:                     curl -i -H 'Content-Type: application/json' -X PUT -d '{"title":"Edited note3","content":"I have been updated"}' http://127.0.0.1:5000//edit_note/3"
   Delete a note(terminal):    curl -X "DELETE" http://127.0.0.1:5000//delete_note/4



"""

if __name__ == '__main__':
    #Test db creation/add/del
    print("running main 1")
